"use strict"
require('dotenv').config()

const CONFIG        = {}
CONFIG.env          = process.env.ENV           || 'development'
CONFIG.port         = process.env.PORT          || '3000'
CONFIG.db_server    = process.env.DB_SERVER     || '127.0.0.1'
CONFIG.db_name      = process.env.DB_NAME       || 'perpus_online_db'
CONFIG.db_user      = process.env.DB_USER       || 'root'
CONFIG.db_password  = process.env.DB_PASS       || '' 
CONFIG.db_connect_wait = process.env.DB_CONNECT_WAIT || true
CONFIG.db_connect_limit = process.env.DB_CONNECT_LIMIT || 10
CONFIG.db_query_limit = process.env.DB_QUERY_LIMIT || 0

module.exports = CONFIG