"use strict"
const express = require('express')

//MIDDLEWARES
const uploadMiddleware = require('../middlewares/upload');

//CONTROLLERS
// first
const dashboardController = require('../controllers/dashboard')

const bookController = require('../controllers/book');
const uploadController = require('../controllers/upload');

//ROUTINGS
module.exports = (app) => {

  //default
  app.get('/', dashboardController.default)

  const bookRouter = express.Router()
  app.use('/books', bookRouter)
  bookRouter.get('/', bookController.getBook)
  bookRouter.post('/', bookController.insertBook)
  bookRouter.put('/:id', bookController.updateBook)
  bookRouter.delete('/:id', bookController.deleteBook)

  const uploadRouter = express.Router()
  app.use('/upload', uploadRouter)
  uploadRouter.post('/image', uploadMiddleware.single('picture'), uploadController.uploadImage)
  uploadRouter.post('/file', uploadMiddleware.single('document'), uploadController.uploadFile)
}