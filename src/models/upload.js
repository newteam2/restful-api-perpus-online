"use strict"

const { Storage } = require("@google-cloud/storage");
const path = require("path");

const storage = new Storage({
  keyFilename: path.join(__dirname, "..", "..", "key.json"),
});

let bucketName = "perpustakaan-online-1590f.appspot.com";

module.exports = {
  uploadImageModel: async (data, callback) => {
    async function generateSignedUrl(filename) {
      // These options will allow temporary read access to the file
      const options = {
        version: "v2", // defaults to 'v2' if missing.
        action: "read",
        expires: "03-09-2491",
      };

      // Get a v2 signed URL for the file
      const [url] = await storage
        .bucket(bucketName)
        .file(filename)
        .getSignedUrl(options);

      callback("", url, "");
    }

    await storage
      .bucket(bucketName)
      .upload(data.path, {
        gzip: true,
        destination: `picture/${data.filename}`,
        metadata: {
          cacheControl: "public, max-age=31536000",
        },
      })
      .then((result) => {
        const file = result[0];
        return file.getMetadata();
      })
      .then((results) => {
        const metadata = results[0];
        console.log(metadata);
        generateSignedUrl(metadata.name).catch((err) => {
          callback(err, "", "");
        });
      })
      .catch((err) => {
        callback(err, "", "");
      });
  },

  uploadFileModel: async (data, callback) => {
    async function generateSignedUrl(filename) {
      // These options will allow temporary read access to the file
      const options = {
        version: "v2", // defaults to 'v2' if missing.
        action: "read",
        expires: "03-09-2491",
      };

      // Get a v2 signed URL for the file
      const [url] = await storage
        .bucket(bucketName)
        .file(filename)
        .getSignedUrl(options);

      callback("", url, "");
    }

    await storage
      .bucket(bucketName)
      .upload(data.path, {
        gzip: true,
        destination: `document/${data.filename}`,
        metadata: {
          cacheControl: "public, max-age=31536000",
        },
      })
      .then((result) => {
        const file = result[0];
        return file.getMetadata();
      })
      .then((results) => {
        const metadata = results[0];
        console.log(metadata);
        generateSignedUrl(metadata.name).catch((err) => {
          callback(err, "", "");
        });
      })
      .catch((err) => {
        callback(err, "", "");
      });
  },
};
