"use strict"

const db = require('../helpers/connect')

const table = 'books'

module.exports = {
  getBookModel: (data, callback) => {
    let sql = `SELECT COUNT(id) AS count FROM ${table} WHERE deleted IN (0)`
    db.query(sql, (err, rowsCount, fields) => {
      if(!err){
        sql = `SELECT * FROM ${table} WHERE deleted IN (0) AND book_name LIKE '%${data.search}%' ORDER BY created_on ${data.sort} LIMIT ${data.limit} OFFSET ${data.offset}`
        db.query(sql, (err, rows, fields) => callback(err, {...rowsCount[0], rows}, fields))
      }
      else{
        callback(err, rows, fields)
      }
    })
  },

  insertBookModel: (data, callback) => {
    let sql = `INSERT INTO ${table} SET book_name='${data.title}', book_image='${data.image}', book_isbn='${data.isbn}', book_description='${data.description}', book_content='${data.content}', publisher='${data.publisher}', published_date='${data.published_date}'`
    db.query(sql, (err, rows, fields) => {
      if(!err){
        sql = `SELECT * FROM ${table} WHERE id=${rows.insertId}`
        db.query(sql, (err, rows, fields) => callback(err, rows, fields))
      }
      else{
        callback(err, rows, fields)
      }
    })
  },

  updateBookModel: (data, callback) => {
    let sql = `UPDATE ${table} SET book_name='${data.title}', book_image='${data.image}', book_isbn='${data.isbn}', book_description='${data.description}', book_content='${data.content}', publisher='${data.publisher}', published_date='${data.published_date}' WHERE id=${data.id}`
    db.query(sql, (err, rows, fields) => {
      if(!err){
        if(rows.affectedRows > 0){
          sql = `SELECT * FROM ${table} WHERE id=${data.id}`
          db.query(sql, (err, rows, fields) => callback(err, rows, fields))
        }
        else{
          callback(404, rows, fields)
        }
      }
      else{
        callback(err, rows, fields)
      }
    })
  },

  deleteBookModel: (data, callback) => {
    let sql = `UPDATE ${table} SET deleted='${data.deleted}' WHERE id=${data.id}`
    db.query(sql, (err, rows, fields) => {
      if(!err){
        if(rows.affectedRows > 0){
          sql = `SELECT * FROM ${table} WHERE id=${data.id}`
          db.query(sql, (err, rows, fields) => callback(err, rows, fields))
        }
        else{
          callback(404, rows, fields)
        }
      }
      else{
        callback(err, rows, fields)
      }
    })
  },
}