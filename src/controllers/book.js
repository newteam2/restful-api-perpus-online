"use strict"

const response = require('../middlewares/response');
const { getBookModel, insertBookModel, updateBookModel, deleteBookModel } = require('../models/book');

module.exports = {
  getBook: (req, res) => {
    let data = {
      search: (req.query.search || ''),
      page: (req.query.page || 1),
      limit: (req.query.limit || 10),
      sort: (req.query.sort || 'desc')
    }

    if(data.page <= 0){
      data.offset = 0
    }
    else{
      data.offset = (data.page - 1) * data.limit
    }

    getBookModel(data, (err, rows, fields) => {
      if(!err){
        let count = rows.count
        let responseData = {
          rows: rows.rows,
          totalBook: count,
          totalPage: Math.ceil(count / data.limit),
          page: data.page,
          limit: data.limit,
          sort: data.sort
        } 
        res.status(200).json(response.render({ data: responseData }))
      }
      else{
        console.log(err)
        res.status(500).json(response.render({ error: 'Connection Failed.' }))
      }
    })
  },

  insertBook: (req, res) => {
    let data = req.body
    
    if(!data.title || !data.publisher || !data.published_date){
      return res.status(400).json(response.render({ error: 'Request not full filled' }))
    }

    insertBookModel(data, (err, rows, fields) => {
      if(!err){
        res.status(200).json(response.render({ data: rows[0] }))
      }
      else{
        console.log(err)
        res.status(500).json(response.render({ error: 'Connection Failed.' }))
      }
    })
    
  },

  updateBook: (req, res) => {
    let data = req.body
    data.id = req.params.id

    if(!data.title || !data.publisher || !data.published_date){
      return res.status(400).json(response.render({ error: 'Request not full filled' }))
    }

    updateBookModel(data, (err, rows, fields) => {
      if(!err){
        res.status(200).json(response.render({ data: rows[0] }))
      }
      else{
        console.log(err)
        if(err == 404){
          return res.status(404).json(response.render({ error: 'Cannot find data with your id' }))
        }
        res.status(500).json(response.render({ error: 'Connection Failed.' }))
      }
    })
  },
  
  deleteBook: (req, res) => {
    let data = req.body
    data.id = req.params.id

    deleteBookModel(data, (err, rows, fields) => {
      if(!err){
        res.status(200).json(response.render({ data: rows[0] }))
      }
      else{
        console.log(err)
        if(err == 404){
          return res.status(404).json(response.render({ error: 'Cannot find data with your id' }))
        }
        res.status(500).json(response.render({ error: 'Connection Failed.' }))
      }
    })
  }
}