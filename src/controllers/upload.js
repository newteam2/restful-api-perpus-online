"use strict"

const response = require("../middlewares/response");
const { uploadImageModel, uploadFileModel } = require("../models/upload");

module.exports = {
  uploadImage: (req, res) => {
    const file = req.file;

    if (!file) {
      return res
        .status(401)
        .json(response.render({ error: "No file is selected" }));
    }

    uploadImageModel(file, (err, rows, fields) => {
      if (!err) {
        let url = rows;
        res.status(200).json(response.render({ data: { url } }));
      } else {
        console.log(err);
        res.status(500).json(response.render({ error: "Connection Failed." }));
      }
    });
  },

  uploadFile: (req, res) => {
    const file = req.file;

    if (!file) {
      return res
        .status(401)
        .json(response.render({ error: "No file is selected" }));
    }

    uploadFileModel(file, (err, rows, fields) => {
      if (!err) {
        let url = rows;
        res.status(200).json(response.render({ data: { url } }));
      } else {
        console.log(err);
        res.status(500).json(response.render({ error: "Connection Failed." }));
      }
    });
  },
};
