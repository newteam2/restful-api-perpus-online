"use strict"

const mysql = require('mysql2');
const config = require('../config');

const db = mysql.createConnection({
	host: config.db_server,
	user: config.db_user,
	password: config.db_password,
	database: config.db_name,
	// waitForConnections: config.db_connection_wait,
	// connectionLimit: config.db_connection_limit,
  // queueLimit: config.db_query_limit
});

db.connect(function(err){
	if(err) throw err;
});

module.exports = db;