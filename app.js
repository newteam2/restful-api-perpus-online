"use strict"
const config  = require('./src/config')

// EXPRESS
const express = require('express')
const app     = express()

// LOGGER FOR DEV
const logger = require('morgan')
if(config.env==='development') {
  app.use(logger('dev'))
}

// I/O
const bodyParser   = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// CORS
var cors = require('cors')
app.use(cors({ origin: true }));
app.options('*', cors());

// CONTROLLERS ROUTE
app.routes = require('./src/routes/v1')(app)

// listen to server with specified port
app.listen(config.port, () => {
  console.log("Server listening at port : " + config.port);
});